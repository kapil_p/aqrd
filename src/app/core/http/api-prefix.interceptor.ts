import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { CredentialsService } from '../authentication/credentials.service';

/**
 * Prefixes all requests not starting with `http[s]` with `environment.serverUrl`.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiPrefixInterceptor implements HttpInterceptor {
  constructor(private credentialsService: CredentialsService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers;

    if (!/^(http|https):/i.test(request.url)) {
      request = request.clone({ url: environment.serverUrl + request.url });
    }

    if (this.credentialsService._credentials != null && this.credentialsService.isAuthenticated) {
      console.log(this.credentialsService);
      headers = new HttpHeaders({
        Authorization: 'Bearer ' + this.credentialsService._credentials.accessToken
        // 'Content-Type': 'application/json'
      });
    }
    request = request.clone({
      url: request.url,
      headers
    });

    return next.handle(request);
  }
}
