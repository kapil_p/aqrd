import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { InstitutesRoutingModule } from './institutes-routing.module';
import { InstitutesComponent } from './institutes.component';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, InstitutesRoutingModule],
  declarations: [InstitutesComponent]
})
export class InstitutesModule {}
