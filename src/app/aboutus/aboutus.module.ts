import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { AboutusRoutingModule } from './aboutus-routing.module';
import { AboutusComponent } from './aboutus.component';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, AboutusRoutingModule],
  declarations: [AboutusComponent]
})
export class AboutusModule {}
