import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { async } from 'q';
// import { shareService } from '@app/shared/services/share.service';
// import { AllCustomerService } from '@app/all-customer-request/all-customer.service';
import { finalize } from 'rxjs/operators';
import { Logger, AuthenticationService } from '@app/core';
// import { NotificationService } from '@app/shared/services/Notification.service';
// import { AuthenticationAccessService } from '@app/core/authentication/authenticationAccess.service';
const log = new Logger('ReportList');
declare var $: any;

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {
  @Input() title: string;
  url: any;
  subscription: Subscription;
  suburl: any;
  active: boolean;
  Role: any;
  count: any = { customerRequestCount: '', subscriberRequestCount: '' };
  interval: any;
  constructor() {
    // this.Role = authenticationAccessService.credentials.user.role;
    // this.subscription = share.subj$.subscribe(async (val: any) => {
    //   await (this.url = val);
    //   if (this.url == 'Home') {
    //     this.suburl = '';
    //     this.url = '';
    //   }
    //   if (this.url == 'All Account List') {
    //     this.suburl = 'suppliers';
    //   }
    //   if (this.url == 'Live Test') {
    //     this.suburl = 'live-no-testing-list';
    //   }
    //   if (this.url == 'All Senders List') {
    //     this.suburl = 'sender-list';
    //   }
    //   if (this.url == 'My Profile') {
    //     this.suburl = 'profile';
    //   }
    //   if (this.url == 'Test Case Create') {
    //     this.suburl = 'live-no-testing';
    //   }
    //   if (this.url == 'Create New Account') {
    //     this.suburl = 'add-supplier';
    //   }
    //   if (this.url == 'Create New Sender') {
    //     this.suburl = 'add-senders';
    //   }
    //   if (this.url == 'Add Balance') {
    //     this.suburl = 'add-balance';
    //   }
    //   if (this.url == 'Transaction Logs') {
    //     this.suburl = 'customer-transaction-history';
    //   }
    //   if (this.url == 'All Customer Request') {
    //     this.suburl = 'all-customer-request';
    //   }
    //   if (this.url == 'All Subscriber Request') {
    //     this.suburl = 'all-subscriber-request';
    //   }
    //   if (this.url == 'Customer Detail') {
    //     this.suburl = 'view-customer-request';
    //   }
    //   if (this.url == 'Customer Logs') {
    //     this.suburl = 'admin-customer-logs';
    //   }
    //   if (this.url == 'Subscriber Logs') {
    //     this.suburl = 'admin-subscriber-logs';
    //   }
    // });
  }

  ngOnInit() {
    if (this.Role === 'admin') {
      this.getCount();
      this.interval = setInterval(() => {
        this.getCount();
      }, 10000);
    }
  
    this.menuJqueryInit();
  }

  menuJqueryInit(){
  
    var $items = $('.nav-main li.nav-parent');

    function expand($li:any) {
      $li.children('ul.nav-children').slideDown('fast', function() {
        $li.addClass('nav-expanded');
        $(this).css('display', '');
        ensureVisible($li);
      });
    }
  
    function collapse($li:any) {
      $li.children('ul.nav-children').slideUp('fast', function() {
        $(this).css('display', '');
        $li.removeClass('nav-expanded');
      });
    }
  
    function ensureVisible($li:any) {
      var scroller = $li.offsetParent();
      if (!scroller.get(0)) {
        return false;
      }
  
      var top = $li.position().top;
      if (top < 0) {
        scroller.animate(
          {
            scrollTop: scroller.scrollTop() + top
          },
          'fast'
        );
      }
    }
  
    function buildSidebarNav(anchor:any, prev:any, next:any, ev:any) {
      if (anchor.prop('href')) {
        var arrowWidth = parseInt(window.getComputedStyle(anchor.get(0), ':after').width, 10) || 0;
        if (ev.offsetX > anchor.get(0).offsetWidth - arrowWidth) {
          ev.preventDefault();
        }
      }
  
      if (prev.get(0) !== next.get(0)) {
        collapse(prev);
        expand(next);
      } else {
        collapse(prev);
      }
    }
  
    $items.find('> a').on('click', function(ev:any) {
      var $html = $('html'),
        $window = $(window),
        $anchor = $(this),
        $prev = $anchor.closest('ul.nav').find('> li.nav-expanded'),
        $next = $anchor.closest('li'),
        $ev = ev;
  
      if ($anchor.attr('href') == '#') {
        ev.preventDefault();
      }
  
      if (!$html.hasClass('sidebar-left-big-icons')) {
        buildSidebarNav($anchor, $prev, $next, $ev);
      } else if ($html.hasClass('sidebar-left-big-icons') && $window.width() < 768) {
        buildSidebarNav($anchor, $prev, $next, $ev);
      }
    });
  
    // Chrome Fix
    $.browser.chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
    if ($.browser.chrome && !$.browser.mobile) {
      var flag = true;
      $('.sidebar-left .nav-main li a').on('click', function() {
        flag = false;
        setTimeout(function() {
          flag = true;
        }, 200);
      });
  
      $('.nano').on('mouseenter', function(e:any) {
        $(this).addClass('hovered');
      });
  
      $('.nano').on('mouseleave', function(e:any) {
        if (flag) {
          $(this).removeClass('hovered');
        }
      });
    }
  
    $('.nav-main a')
      .filter(':not([href])')
      .attr('href', '#');

  }

  getCount() {
    // this.allCustomerService
    //   .getCount()
    //   .pipe(finalize(() => {}))
    //   .subscribe(
    //     (response: any) => {
    //       this.count = response;
    //       this.notificationService.setOption(this.count);
    //     },
    //     (error: {}) => {
    //       log.debug(`Forget Password error: ${error}`);
    //     }
    //   );
  }

  /* Destroy time interval*/
  // ngOnDestroy() {
  //   clearInterval(this.interval);
  // }
}
