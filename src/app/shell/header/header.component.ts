import { Component, OnInit, NgZone, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService, I18nService, Logger } from '@app/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
const log = new Logger('ReportList');

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  name: any;
  balance: any;
  public now: Date = new Date();
  digitalClock: any;
  subscription: Subscription;
  profile: any;
  credit_balance: any;
  Role: any;
  Credentials: any;
  @Input() data: any;
  question: any = {};
  count: any = { customerRequestCount: 0, subscriberRequestCount: 0 };
  interval: any;
  interval1: any;
  constructor() {
    // this.getProfile();
    // this.interval = setInterval(() => {
    //   this.count = notificationService.getOption();
    // }, 10000);
    // this.Role = authenticationAccessService.credentials.user.role;
    // this.Credentials = authenticationAccessService.credentials;
    // this.zone.runOutsideAngular(() => {
    //   setInterval(() => {
    //     this.digitalClock = this.now.toUTCString();
    //   }, 1);
    // });
    // this.name = authenticationAccessService.credentials.user.name;
    // this.balance = authenticationService.credentials.user.credit_balance;
    // if (this.Role === 'user') {
    //   // this.interval1 === setInterval(() => {
    //   //   // this.getProfile();
    //   // }, 10000);
    // }
  }

  ngOnInit() {
    setInterval(() => {
      this.now = new Date();
    }, 1);

    // this.subscription = this.ShareBalanceService.subj$.subscribe(async (val: any) => {
    //   this.getProfile();
    // });
  }

  // public getProfile() {
  //   this.authenticationService
  //     .getProfile()
  //     .pipe(finalize(() => {}))
  //     .subscribe(
  //       (response: any) => {
  //         this.profile = response;
  //         this.balance = this.profile.credit_balance;
  //       },
  //       (error: {}) => {
  //         log.debug(`error: ${error}`);
  //       }
  //     );
  // }

  logout() {
    // this.authenticationAccessService.logout().subscribe(() =>
    // this.router.navigate(['/login'], { replaceUrl: true }));
  }

  // ngOnDestroy() {
  //   clearInterval(this.interval);
  //   clearInterval(this.interval1);
  // }

  get isAdmin() {
    return this.Credentials && this.Role === 'admin';
  }

  // get email(): string | null {
  //   // const credentials = this.authenticationAccessService.credentials;
  //   // return credentials ? credentials.user.email : null;
  // }
}
