import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { SharedModule } from '@app/shared';
import { UsersModule } from './users/users.module';

@NgModule({
  imports: [CommonModule, TranslateModule, SharedModule, NgbModule, RouterModule, UsersModule],
  declarations: [HeaderComponent, ShellComponent, SideMenuComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShellModule {}
