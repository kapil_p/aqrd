import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { Users } from '../users.constants';
import { map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UsersService {
  public users: Users[] = [];
  constructor(private httpClient: HttpClient) {}

  addUser(data: any, editMode: boolean) {
    if (editMode) {
      const index = this.users.findIndex(u => u.id == Number(data.id));
      this.users[index] = data;
      return of(data);
    } else {
      delete data.confirmPassword;
      delete data.department;
      delete data.designation;
      delete data.id;
      delete data.yearOfJoining;
      const updatedData = {
        ...data,
        Username: 'abc@amp.com',
        CityId: 'C11CA8F2-901C-49D0-07C4-08D7D3F7D6EB',
        CountryId: '00000000-0000-0000-0000-000000000002',
        Website: 'www.abc.com',
        MiddleName: 'Hanumant',
        Gender: '0',
        PinCode: 411028,
        Biography: 'cool dude',
      };
      return this.httpClient
        .cache()
        .post(`/Account/CreateUser`, updatedData)
        .pipe(
          map((body: any) => body),
          catchError(this.handleError)
        );
    }
  }

  updateUser(data: any) {
    return of(data);
  }

  findUserById(id: any) {
    const user = this.users.find(u => u.id == Number(id));
    return of(user);
  }

  deleteUser(id: any) {
    return this.httpClient
      .cache()
      .get(`/Account/DeleteUser?id=${id}`)
      .pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  getUsers(searchKey:  string, page: number, limit: number) {
    return this.httpClient
      .cache()
      .get(`/Account/LoadUsers?SearchKey=${searchKey}&Page=${page}&Limit=${limit}`)
      .pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  getInstitutes() {
    return this.httpClient
      .cache()
      .get(`/Institute/GetInstitutes?SearchKey=&Page=0&Limit=100`)
      .pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
    }
    return throwError(error || 'Node.js server error');
  }
}
