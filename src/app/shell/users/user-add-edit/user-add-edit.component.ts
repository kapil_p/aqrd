import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MustMatch } from '../must-match.validator';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { ROLES, YEARS, Users } from '../users.constants';
import { UsersService } from './../service/users.service';

@Component({
  selector: 'app-user-add-edit',
  templateUrl: './user-add-edit.component.html',
  styleUrls: ['./user-add-edit.component.scss']
})
export class UserAddEditComponent implements OnInit, OnDestroy {
  form: any;
  roles = ROLES;
  institutes: any = [];
  years = YEARS;
  submitted = false;
  editMode = false;
  unSubscribe = new Subject();
  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group(
      {
        id: new FormControl(null),
        // Username: new FormControl(null, {
        //   validators: [Validators.required, Validators.email]
        // }),
        Email: new FormControl(null, {
          validators: [Validators.required, Validators.email]
        }),
        RoleName: new FormControl(null, {
          validators: [Validators.required]
        }),
        FirstName: new FormControl(null, {
          validators: [Validators.required]
        }),
        InstituteId: new FormControl(null, {
          validators: [Validators.required]
        }),
        LastName: new FormControl(null, {
          validators: [Validators.required]
        }),
        department: new FormControl(null, {
          validators: [Validators.required]
        }),
        Password: new FormControl(null, {
          validators: [Validators.required]
        }),
        designation: new FormControl(null, {
          validators: [Validators.required]
        }),
        confirmPassword: new FormControl(null, {
          validators: [Validators.required]
        }),
        yearOfJoining: new FormControl(null, {
          validators: [Validators.required]
        }),
        PhoneNumber: new FormControl(null, {
          validators: [Validators.required]
        })
      },
      { validator: MustMatch('Password', 'confirmPassword') }
    );
    this.form.valueChanges.subscribe(() => {
      this.submitted = false;
    });

    this.activatedRoute.params.pipe(takeUntil(this.unSubscribe)).subscribe((d: Params) => {
      if (d.id) {
        this.editMode = true;
        this.usersService
          .findUserById(d.id)
          .pipe(takeUntil(this.unSubscribe))
          .subscribe(
            (user: Users) => {
              if (!user) {
                return this.router.navigate(['/users']);
              }
              Object.keys(user).map(userKey => {
                this.form.patchValue({
                  [userKey]: user[userKey]
                });
              });
            },
            e => {
              this.router.navigate(['/users']);
            }
          );
      }
    });

    this.usersService.getInstitutes()
    .subscribe(d => {
      this.institutes = d.items;
    }, e => {
      console.log('e', e);
    });
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.usersService
      .addUser(this.form.value, this.editMode)
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(
        d => {
          this.router.navigate(['/users']);
        },
        e => {}
      );
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }
}
