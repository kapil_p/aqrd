export interface Users {
  id: string;
  name: string;
  email: string;
  institute: string;
  phone: string;
  role: number;
  confirmPassword?: string;
  department?: string;
  designation?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
  yearOfJoining?: string;
  isActive: boolean;
  gender?: string;
  phoneNumber?: string;
  roleName?: string;
  username?: string;
}

export const ROLES: any = [
  {
    id: 1,
    name: 'User'
  }
];

export const YEARS: any = [
  {
    id: 1,
    name: '2019'
  },
  {
    id: 2,
    name: '2020'
  }
];
