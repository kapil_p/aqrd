import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Users } from '../users.constants';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { UsersService } from '../service/users.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  loading = true;
  dataSource: any;
  displayedColumns: string[] = ['id', 'name', 'email', 'phone', 'institute', 'role', 'actions'];
  filters = {
    pageIndex: 0,
    length: 0,
    pageSize: 2,
    searchBy: ''
  };
  pageSizeOptions: number[] = [10, 20, 50, 100, 200, 300];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  unSubscribe = new Subject();
  constructor(private usersService: UsersService) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.loading = true;
    this.usersService
      .getUsers(this.filters.searchBy, this.filters.pageIndex, this.filters.pageSize)
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(d => {
        this.loading = false;
        this.dataSource = new MatTableDataSource<Users>(d.items);
        this.filters.length = d.totalItems;
      });
  }

  onPaginateChange($event: any) {
    this.filters.pageIndex = $event.pageIndex
    this.filters.pageSize = $event.pageSize,
    this.getUsers();
  }

  searchUser(key: string) {
    this.filters.searchBy = key;
    this.getUsers();
  }

  removeUser(id: any) {
    this.usersService
      .deleteUser(id)
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(d => {
        this.getUsers();
      });
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }
}
