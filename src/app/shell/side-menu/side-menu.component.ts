import { Component, OnInit } from '@angular/core';
import { AuthenticationService, Logger } from '@app/core';
import { UserSideMenu } from 'src/constants/constant';

// import { NotificationService } from '@app/shared/services/Notification.service';
// import { AuthenticationAccessService } from '@app/core/authentication/authenticationAccess.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  allcustomerRequestList: any;
  sidemenuList: any;
  Role: any;
  Credentials: any;
  constructor(
    private authenticationService: AuthenticationService
  ) {
    // this.Role = authenticationAccessService.credentials.user.role;
    // this.Credentials = authenticationAccessService.credentials;
  }

  ngOnInit() {
    this.sidemenuList = UserSideMenu.arr;
  }

  get isAdmin() {
    return this.Credentials && this.Role === 'admin';
  }
}
