// my-dialog-component.ts
import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-reject-reason-dialog-modal',
    templateUrl: './reject-reason-dialog-modal.component.html',
    styleUrls: ['./reject-reason-dialog-modal.component.css']
})
export class RejectReasonModalComponent implements OnInit {


    fromDialog: string;


    constructor(
        @Optional() public dialogRef: MatDialogRef<RejectReasonModalComponent>) { }

    onNoClick(): void {
        this.dialogRef.close({ event: 'close', data: this.fromDialog });
    }

    ngOnInit() {
    }

    closeDialog() {
        this.dialogRef.close({ event: 'close', data: this.fromDialog });
    }

    saveDialog() {
        this.dialogRef.close({ event: 'save', data: this.fromDialog });
    }

}