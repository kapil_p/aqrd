import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const routes = {
  getDashBoardStats: () => `/Dashboard/GetDashBoardStats`,
  GetDashBoardBookingsByWeek: () => `/Dashboard/GetDashBoardBookingsByWeek`,
  GetDashBoardBookingsInDuration: (c: ChartDuration) => `/Dashboard/GetDashBoardBookingsInDuration?StartDate=${c.startDate}&EndDate=${c.endDate}`,
  GetDashBoardBookingsByYear: () => `/Dashboard/GetDashBoardBookingsByYear?year=${(new Date()).getFullYear()}`,
  GetBookings: (c: GetBookingsContext) => `/Booking/GetBookings?SearchKey=${(c.searchkey)}&Page=${(c.Page)}&Limit=${(c.Limit)}`,
  RejectBooking: () => `/Booking/RejectBooking`,
  AcceptBooking: () => `/Booking/AcceptBooking`,
};


export interface AcceptBookingContext {
  BookingId: any;
}

export interface RejectBookingContext {
  BookingId: any;
  BookingRejectedReason: any;
}

export interface GetBookingsContext {
  searchkey: string;
  Page: number;
  Limit: number;
}

export interface Booking {
  id: string;
  equipmentId: string;
  equipmentName: string;
  startDateTime: Date;
  endDateTime: Date;
  bookedBy: string;
  bookingStatus: string;
  rejectedBy: string;
  acceptedBy?: any;
  bookingRejectedReason?: any;
  bookingRejectedDate?: any;
  bookingApprovedDate: Date;
  bookingCancelDate?: any;
  bookingTypeName: string;
}

export interface DashboardStat {
  bookedEquipmentsToday: number;
  requestedEquipmentsToday: number;
  rejectedEquipmentsToday: number;
  totalEquipmentAvailable: number;
  totalInstitutes: number;
}

export interface ChartDuration {
  startDate: string;
  endDate: string;
}

export interface DashboardStatByWeek {
  date: string;
  numberOfBookings: number;
}

export interface GetDashBoardBookingsByYear {
  month: string;
  numberOfBookings: number;
}


@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(private httpClient: HttpClient) { }

  getDashBoardStats(): Observable<DashboardStat> {
    //console.log(context);
    return this.httpClient
      .cache()
      .get(routes.getDashBoardStats()).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  GetDashBoardBookingsByWeek(): Observable<DashboardStatByWeek[]> {
    //console.log(context);
    return this.httpClient
      .cache()
      .get(routes.GetDashBoardBookingsByWeek()).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  getDashBoardBookingsInDuration(context: ChartDuration): Observable<DashboardStatByWeek[]> {
    //console.log(context);
    return this.httpClient
      .cache()
      .get(routes.GetDashBoardBookingsInDuration(context)).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  GetBookings(context: GetBookingsContext): Observable<any> {
    //console.log(context);
    return this.httpClient
      .cache()
      .get(routes.GetBookings(context)).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  getDashBoardBookingsByYear(): Observable<GetDashBoardBookingsByYear[]> {
    //console.log(context);
    return this.httpClient
      .cache()
      .get(routes.GetDashBoardBookingsByYear()).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  acceptBooking(context: AcceptBookingContext): Observable<string> {

    const payload = new HttpParams()
      .set('BookingId', context.BookingId);

    return this.httpClient
      .cache()
      .post(routes.AcceptBooking(), payload).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  rejectBooking(context: RejectBookingContext): Observable<GetDashBoardBookingsByYear[]> {

    const payload = new HttpParams()
      .set('BookingId', context.BookingId)
      .set('BookingRejectedReason', context.BookingRejectedReason);


    return this.httpClient
      .cache()
      .post(routes.RejectBooking(), payload).pipe(
        map((body: any) => body),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
    }
    return throwError(error || 'Node.js server error');
  }

}
