import { Component, OnInit, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { HomeService, DashboardStat, Booking } from './home.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Logger } from '@app/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { RejectReasonModalComponent } from './RejectReasonModalComponent';
declare let $: any;

const log = new Logger('Dashboard');


// export interface PeriodicElement {
//   equipment: string;
//   id: number;
//   username: number;
//   timeslot: string;
// }


// const ELEMENT_DATA: PeriodicElement[] = [
//   { id: 1, equipment: 'Hydrogen', username: 1.0079, timeslot: 'H' },
//   { id: 2, equipment: 'Helium', username: 4.0026, timeslot: 'He' },
//   { id: 3, equipment: 'Lithium', username: 6.941, timeslot: 'Li' },
//   { id: 4, equipment: 'Beryllium', username: 9.0122, timeslot: 'Be' },
//   { id: 5, equipment: 'Boron', username: 10.811, timeslot: 'B' },
//   { id: 6, equipment: 'Carbon', username: 12.0107, timeslot: 'C' },
//   { id: 7, equipment: 'Nitrogen', username: 14.0067, timeslot: 'N' },
//   { id: 8, equipment: 'Oxygen', username: 15.9994, timeslot: 'O' },
//   { id: 9, equipment: 'Fluorine', username: 18.9984, timeslot: 'F' },
//   { id: 10, equipment: 'Neon', username: 20.1797, timeslot: 'Ne' },
//   { id: 11, equipment: 'Sodium', username: 22.9897, timeslot: 'Na' },
//   { id: 12, equipment: 'Magnesium', username: 24.305, timeslot: 'Mg' },
//   { id: 13, equipment: 'Aluminum', username: 26.9815, timeslot: 'Al' },
//   { id: 14, equipment: 'Silicon', username: 28.0855, timeslot: 'Si' },
//   { id: 15, equipment: 'Phosphorus', username: 30.9738, timeslot: 'P' },
//   { id: 16, equipment: 'Sulfur', username: 32.065, timeslot: 'S' },
//   { id: 17, equipment: 'Chlorine', username: 35.453, timeslot: 'Cl' },
//   { id: 18, equipment: 'Argon', username: 39.948, timeslot: 'Ar' },
//   { id: 19, equipment: 'Potassium', username: 39.0983, timeslot: 'K' },
//   { id: 20, equipment: 'Calcium', username: 40.078, timeslot: 'Ca' },
// ];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  quote: string | undefined;

  isLoading = false;

  error: string | undefined;

  bookingChartKey: number | 0;

  dashboardStat: DashboardStat;

  displayedColumns: string[] = ['id', 'equipment', 'username', 'timeslot', 'status', 'actions'];

  dataSource: any = null;

  charData: any = [];

  bookingData: Booking[] = [];

  bookingTotaldata: number;

  length = 10;

  pageSize = 10;

  pageSizeOptions: number[] = [10, 20, 50];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private homeService: HomeService, private toastr: ToastrService, public dialog: MatDialog) { }

  // MatPaginator Output
  pageEvent: PageEvent;

  onPaginateChange(event: any) {
    this.getBookingForDashboard("", event.pageIndex, event.pageSize);
  }
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  getColorForActionButton(status: string) {

    switch (status) {
      case "Booked":
        return "primary";

      case "Requested":
        return "white";

      case "Rejected":
        return "warn";

      default:
        return "white";
    }

  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  ngOnInit() {
    this.isLoading = true;

    this.getDataForDashboardStat();

    this.getBookingForDashboard("", 0, 10);

  }

  acceptBookings(bookingID: string, index: number) {

    this.homeService.acceptBooking(
      {
        BookingId: bookingID
      }
    ).pipe(
      finalize(() => {
      }))
      .subscribe(
        response => {
          this.bookingData[index].bookingStatus = "Booked";

          this.dataSource = new MatTableDataSource<Booking>(this.bookingData);

          this.toastr.success('Booking Accepted');
        },
        error => {
          console.log(error);
          this.toastr.error(error.error);
          log.debug(`Search Result error: ${error}`);
          this.error = error;
        }
      );

  }

  rejectBookings(bookingID: string, index: number) {

    // { 'BookingId': boookingId, 'BookingRejectedReason': rejectReason }
    const dialogRef = this.dialog.open(RejectReasonModalComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result.event == "save") {

        console.log(result.data);

        this.homeService.rejectBooking(
          {
            BookingId: bookingID,
            BookingRejectedReason: result.data
          }
        ).pipe(
          finalize(() => {
          }))
          .subscribe(
            response => {
              this.bookingData[index].bookingStatus = "Rejected";

              this.dataSource = new MatTableDataSource<Booking>(this.bookingData);

              this.toastr.success('Booking Rejected');
            },
            error => {
              console.log(error);
              this.toastr.error(error.error);
              log.debug(`Search Result error: ${error}`);
              this.error = error;
            }
          );
      }

    });

  }

  selectChart(chartNumber: number) {
    this.bookingChartKey = chartNumber;
    switch (chartNumber) {
      case 0:
        this.drawChartForYearData()
        break;

      case 1:
        // this.drawChartForMonthData()
        break;

      case 2:
        this.drawChartForWeekData()
        break;

      default:
        break;
    }
  }

  getBookingForDashboard(searchkey: string, page: number, limit: number) {
    // API error for pagination
    this.homeService.GetBookings({
      searchkey: searchkey,
      Page: page,
      Limit: limit
    }).pipe(
      finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        response => {
          this.bookingData = response.items;

          this.dataSource = new MatTableDataSource<Booking>(this.bookingData);

          this.length = response.totalItems;

          this.dataSource.paginator = this.paginator;

          this.selectChart(0);

        },
        error => {
          log.debug(`Search Result error: ${error}`);
          this.error = error;
        }
      );
  }

  getDataForDashboardStat() {
    this.homeService.getDashBoardStats().pipe(
      finalize(() => {
        // this.isLoading = false;
      }))
      .subscribe(
        response => {
          this.dashboardStat = response;
        },
        error => {
          log.debug(`Search Result error: ${error}`);
          this.error = error;
        }
      );
  }

  drawChartForYearData() {
    this.charData = [];
    this.homeService.getDashBoardBookingsByYear().pipe(
      finalize(() => {
        // this.isLoading = false;
      }))
      .subscribe(
        response => {

          for (let item of response) {
            let tempArray = [];

            // var iteamDate = new Date(item.month);

            tempArray.push(item.month);
            tempArray.push(item.numberOfBookings);

            this.charData.push(tempArray);

          }

          this.drawChart();

        },
        error => {
          log.debug(`Search Result error: ${error}`);
          this.error = error;
        }
      );

  }

  // API need to update
  drawChartForMonthData() {
    this.charData = [];
    var now = new Date();

    var firstDate = 1 + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
    var currentDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();

    this.homeService.getDashBoardBookingsInDuration({
      startDate: firstDate,
      endDate: currentDate
    }).pipe(
      finalize(() => {
        // this.isLoading = false;
      }))
      .subscribe(
        response => {

          for (let item of response) {
            let tempArray = [];

            var iteamDate = new Date(item.date);

            tempArray.push(iteamDate.getDate());
            tempArray.push(item.numberOfBookings);

            this.charData.push(tempArray);

          }

          this.drawChart();

        },
        error => {
          log.debug(`Search Result error: ${error}`);
          this.error = error;
        }
      );

  }

  drawChartForWeekData() {
    this.charData = [];
    this.homeService.GetDashBoardBookingsByWeek().pipe(
      finalize(() => {
        // this.isLoading = false;
      }))
      .subscribe(
        response => {

          for (let item of response) {
            let tempArray = [];

            var iteamDate = new Date(item.date);

            tempArray.push(iteamDate.getDate());
            tempArray.push(item.numberOfBookings);

            this.charData.push(tempArray);

          }

          this.drawChart();

        },
        error => {
          log.debug(`Search Result error: ${error}`);
          this.error = error;
        }
      );

  }

  drawChart() {
    if ($('#flotDashBasic').get(0)) {
      var flotDashBasicData = [{
        data: this.charData,
        label: "Bookings",
        color: "#7d3859"
      }];

      $.plot('#flotDashBasic', flotDashBasicData, {
        series: {
          lines: {
            show: true,
            fill: false,
            lineWidth: 3,
            fillColor: {
              colors: [
                {
                  opacity: 0.45
                },
                {
                  opacity: 0.45
                }
              ]
            }
          },
          points: {
            show: true
          },
          shadowSize: 0
        },
        grid: {
          hoverable: true,
          clickable: true,
          borderColor: 'rgba(0,0,0,0.1)',
          borderWidth: 1,
          labelMargin: 15,
          backgroundColor: 'transparent'
        },
        tooltip: true,
        tooltipOpts: {
          content: '%s: Value of %x is %y',
          shifts: {
            x: -60,
            y: 25
          },
          defaultTheme: false
        }
      });
    }
  }

  scriptForCorossal() {
    // Carousel

    $('[data-plugin-carousel]').each(function () {
      const $this = $(this);
      let opts = {};

      const pluginOptions = $this.data('plugin-options');
      if (pluginOptions) {
        opts = pluginOptions;
      }

      $this.themePluginCarousel(opts);
    });

  }
}
