import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { EquipmentRoutingModule } from './equipment-routing.module';
import { EquipmentComponent } from './equipment.component';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, EquipmentRoutingModule],
  declarations: [EquipmentComponent]
})
export class EquipmentModule {}
