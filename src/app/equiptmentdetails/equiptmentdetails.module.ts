import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { EquipmentdetailsRoutingModule } from './equiptmentdetails-routing.module';
import { EquipmentdetailsComponent } from './equiptmentdetails.component';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, EquipmentdetailsRoutingModule],
  declarations: [EquipmentdetailsComponent]
})
export class EquipmentdetailsModule {}
