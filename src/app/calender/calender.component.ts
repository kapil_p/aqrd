import { Component, ViewChild, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction'; // for dateClick
// declare var $: any;
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'

// import { QuoteService } from './quote.service';

@Component({
  selector: 'app-home',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss']
})
export class CalenderComponent implements OnInit {
  // calendarPlugins = [dayGridPlugin]; // important!
  quote: string | undefined;
  isLoading = false;

  @ViewChild('calendar', { static: false }) calendarComponent: FullCalendarComponent; // the #calendar in the template

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [{ title: 'Event Now', start: new Date() }];

  constructor() { }

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    const calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  }

  handleDateClick(arg: any) {
    // $('.modal-with-zoom-anim').trigger('click');
    // // if (confirm('Would you like to add an event to ' + arg.dateStr + ' ?')) {
    // //   this.calendarEvents = this.calendarEvents.concat({
    // //     // add new event data. must create new array
    // //     title: 'New Event',
    // //     start: arg.date,
    // //     allDay: arg.allDay
    // //   });
    // // }
  }

  ngOnInit() {
    /*
Modal with CSS animation
*/
    // $('.modal-with-zoom-anim').magnificPopup({
    //   type: 'inline',

    //   fixedContentPos: false,
    //   fixedBgPos: true,

    //   overflowY: 'auto',

    //   closeBtnInside: true,
    //   preloader: false,

    //   midClick: true,
    //   removalDelay: 300,
    //   mainClass: 'my-mfp-zoom-in',
    //   modal: true
    // });
    // /* tslint:disable */

    // $(document).on('click', '.modal-dismiss', function (e: any) {
    //   e.preventDefault();
    //   $.magnificPopup.close();
    // });

    // /*
    // Modal Confirm
    // */
    // $(document).on('click', '.modal-confirm', function (e: any) {
    //   e.preventDefault();
    //   $.magnificPopup.close();

    //   Swal.fire(
    //     'Sucess!',
    //     'Your request sent to the asset admin. Once accept your booking will be completed.',
    //     'success'
    //   )

    //   // new PNotify({
    //   //   title: 'Success!',
    //   //   text: 'Modal Confirm Message.',
    //   //   type: 'success'
    //   // });
    // });

    /* tslint:enable */
    // this.isLoading = true;
    // this.quoteService.getRandomQuote({ category: 'dev' })
    //   .pipe(finalize(() => { this.isLoading = false; }))
    //   .subscribe((quote: string) => { this.quote = quote; });
  }


}
