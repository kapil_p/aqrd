type UserSideMenuList = Array<{ id: number; Title: string; routerLink: any; Icon: string }>;

export class Constants {
  static DATE_FMT = 'y-MM-dd/hh:mm:ss';
  static DATE_TIME_FMT = `${Constants.DATE_FMT}`;
}

export class UserSideMenu {
  static arr: UserSideMenuList = [
    { id: 1, Title: 'Dashboard', routerLink: '/home', Icon: 'fa fa-home' },
    { id: 2, Title: 'Account', routerLink: '/suppliers', Icon: 'fa fa-exchange' },
    { id: 3, Title: 'Live Number Testing', routerLink: '/live-no-testing-list', Icon: 'fa fa-bug' },
    { id: 4, Title: 'Senders', routerLink: '/sender-list', Icon: 'fa fa-cogs' },
    { id: 5, Title: 'Logs', routerLink: '/customer-transaction-history', Icon: 'fa fa-history' }
  ];
}
